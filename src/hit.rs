use crate::vector::*;
use crate::Material;
use crate::Ray;

pub struct Sphere {
    pub center: Point,
    pub radius: f64,
    pub material: Material,
}

pub struct HitRecord {
    pub normal: Vector,
    pub point: Point,
    t: f64,
    pub material: Material,
}

pub trait Hittable {
    fn hit(&self, ray: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord>;
}

impl Hittable for Sphere {
    fn hit(&self, ray: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let oc = ray.origin - self.center;
        let a = ray.direction.length_squared();
        let b = dot_product(&oc, &ray.direction);
        let c = oc.length_squared() - self.radius.powi(2);
        let discriminant = b.powi(2) - a * c;

        if discriminant < 0.0 {
            return None;
        }

        let mut root = (-b - discriminant.sqrt()) / a;

        if root < t_min || root > t_max {
            root = (-b + discriminant.sqrt()) / a;

            if root < t_min || root > t_max {
                return None;
            }
        }

        let hit_point = ray.at(root);

        Some(HitRecord {
            t: root,
            point: hit_point,
            normal: (hit_point - self.center).unit_vector(),
            material: self.material,
        })
    }
}

impl<T: Hittable> Hittable for &[T] {
    fn hit(&self, ray: &Ray, t_min: f64, t_max: f64) -> Option<HitRecord> {
        let mut hit_record = None;
        let mut closest = t_max;

        for object in *self {
            if let Some(hit) = object.hit(ray, t_min, closest) {
                closest = hit.t;
                hit_record = Some(hit);
            }
        }

        hit_record
    }
}
