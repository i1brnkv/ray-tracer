pub type Point = Vector;

#[derive(Copy, Clone)]
pub struct Vector {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Vector {
    pub fn length_squared(&self) -> f64 {
        self.x.powi(2) + self.y.powi(2) + self.z.powi(2)
    }

    pub fn length(&self) -> f64 {
        self.length_squared().sqrt()
    }

    pub fn unit_vector(&self) -> Vector {
        *self / self.length()
    }

    pub fn near_zero(&self) -> bool {
        let s: f64 = 1e-10;

        self.x.abs() < s && self.y.abs() < s && self.z.abs() < s
    }
}

pub fn dot_product(v: &Vector, u: &Vector) -> f64 {
    v.x * u.x + v.y * u.y + v.z * u.z
}

pub fn cross_product(v: &Vector, u: &Vector) -> Vector {
    Vector {
        x: v.y * u.z - v.z * u.y,
        y: v.z * u.x - v.x * u.z,
        z: v.x * u.y - v.y * u.x,
    }
}

use std::ops::Mul;

impl Mul<Vector> for f64 {
    type Output = Vector;

    fn mul(self, other: Vector) -> Vector {
        (other.x * self, other.y * self, other.z * self).into()
    }
}

use std::ops::Div;

impl Div<f64> for Vector {
    type Output = Self;

    fn div(self, other: f64) -> Self {
        (self.x / other, self.y / other, self.z / other).into()
    }
}

use std::ops::Add;

impl Add for Vector {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        (self.x + other.x, self.y + other.y, self.z + other.z).into()
    }
}

use std::ops::Sub;

impl Sub for Vector {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        (self.x - other.x, self.y - other.y, self.z - other.z).into()
    }
}

use rand::distributions::{Distribution, Standard};
use rand::Rng;

impl Distribution<Vector> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Vector {
        Vector {
            x: rng.gen_range(-1.0..=1.0),
            y: rng.gen_range(-1.0..=1.0),
            z: rng.gen_range(-1.0..=1.0),
        }
    }
}

use std::ops::Neg;

impl Neg for Vector {
    type Output = Self;

    fn neg(self) -> Self {
        (-self.x, -self.y, -self.z).into()
    }
}

impl From<(f64, f64, f64)> for Vector {
    fn from(e: (f64, f64, f64)) -> Self {
        Vector {
            x: e.0,
            y: e.1,
            z: e.2,
        }
    }
}
