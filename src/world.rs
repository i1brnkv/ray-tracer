use crate::vector::*;
use crate::Color;
use crate::Material;
use crate::Sphere;
use rand::{Rng, SeedableRng};

extern crate rand_xorshift;
use rand_xorshift::XorShiftRng;

pub type World = Vec<Sphere>;

pub fn spheres_scene() -> World {
    let mut world = vec![
        Sphere {
            center: (0.0, -1000.0, 0.0).into(),
            radius: 1000.0,
            material: Material::Lambertian {
                albedo: (0.5, 0.5, 0.5).into(),
            },
        },
        Sphere {
            center: (-4.0, 1.0, 0.0).into(),
            radius: 1.0,
            material: Material::Lambertian {
                albedo: (0.4, 0.2, 0.1).into(),
            },
        },
        Sphere {
            center: (0.0, 1.0, 0.0).into(),
            radius: 1.0,
            material: Material::Dielectric {
                refraction_idx: 1.6,
            },
        },
        Sphere {
            center: (4.0, 1.0, 0.0).into(),
            radius: 1.0,
            material: Material::Metal {
                albedo: (0.7, 0.6, 0.5).into(),
                fuzz: 0.0,
            },
        },
    ];

    let mut rng = XorShiftRng::seed_from_u64(0);

    for a in -11..11 {
        for b in -11..11 {
            let mut sphere_radius: f64;
            let mut sphere_center: Point;

            'sphere_gen: loop {
                sphere_radius = rng.gen_range(0.1..0.3);
                sphere_center = Point {
                    x: a as f64 + 0.9 * rng.gen::<f64>(),
                    y: sphere_radius,
                    z: b as f64 + 0.9 * rng.gen::<f64>(),
                };

                for s in &world {
                    if (s.center - sphere_center).length()
                        < (s.radius + sphere_radius)
                    {
                        continue 'sphere_gen;
                    }
                }
                break;
            }

            let sphere_material: Material;
            let choose_material: f64 = rng.gen();

            if choose_material < 0.8 {
                // diffuse
                sphere_material = Material::Lambertian {
                    albedo: Color {
                        r: rng.gen::<f64>() * rng.gen::<f64>(),
                        g: rng.gen::<f64>() * rng.gen::<f64>(),
                        b: rng.gen::<f64>() * rng.gen::<f64>(),
                    },
                };
            } else if choose_material < 0.95 {
                // metal
                sphere_material = Material::Metal {
                    albedo: Color {
                        r: rng.gen_range(0.5..1.0),
                        g: rng.gen_range(0.5..1.0),
                        b: rng.gen_range(0.5..1.0),
                    },
                    fuzz: rng.gen_range(0.0..0.5),
                };
            } else {
                // glass
                sphere_material = Material::Dielectric {
                    refraction_idx: 1.5,
                };
            }

            world.push(Sphere {
                center: sphere_center,
                radius: sphere_radius,
                material: sphere_material,
            });
        }
    }

    world
}
