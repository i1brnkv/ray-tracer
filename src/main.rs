mod vector;
use crate::vector::*;

mod ray;
use crate::ray::*;

mod color;
use crate::color::*;

mod hit;
use crate::hit::*;

mod camera;
use crate::camera::*;

mod material;
use crate::material::*;

mod world;
use crate::world::*;

extern crate rand;

#[macro_use]
extern crate clap;
use clap::Arg;

extern crate image;
use image::RgbImage;

fn parse_args() -> (u32, u32, f64, usize, usize, String) {
    let matches = app_from_crate!()
        .arg(
            Arg::with_name("width")
                .long("width")
                .takes_value(true)
                .help("image width in pixels, if not set will be \
                    `1.5 * height`, if both not set will be 900")
        )
        .arg(Arg::with_name("height")
                .long("height")
                .takes_value(true)
                .help("image height in pixels, if not set will be \
                    `width / 1.5`, if both not set will be 600")
        )
        .arg(Arg::with_name("fov")
                .long("fov")
                .takes_value(true)
                .default_value("22.0")
                .help("vertical angle of view")
        )
        .arg(Arg::with_name("samples")
                .long("samples")
                .takes_value(true)
                .default_value("10")
                .help("number of rays per pixel")
        )
        .arg(Arg::with_name("depth")
                .long("depth")
                .takes_value(true)
                .default_value("50")
                .help(
                "maximum number of reflections to calculate for a single ray",
            )
        )
        .arg(Arg::with_name("out_file")
                .long("out")
                .takes_value(true)
                .default_value("out.png")
                .help("specify file to save the image in common format")
        )
        .get_matches();

    let vfov = value_t!(matches, "fov", f64).unwrap_or_else(|e| e.exit());
    let samples_per_pixel =
        value_t!(matches, "samples", usize).unwrap_or_else(|e| e.exit());
    let max_depth =
        value_t!(matches, "depth", usize).unwrap_or_else(|e| e.exit());
    let out_file_path = matches.value_of("out_file").unwrap().to_string();

    let width = value_t!(matches, "width", u32);
    let height = value_t!(matches, "height", u32);

    let aspect_ratio = 1.5;
    let image_width;
    let image_height;

    if matches.is_present("width") {
        image_width = width.unwrap_or_else(|e| e.exit());

        if matches.is_present("height") {
            image_height = height.unwrap_or_else(|e| e.exit());
        } else {
            image_height = (image_width as f64 / aspect_ratio) as u32;
        }
    } else if matches.is_present("height") {
        image_height = height.unwrap_or_else(|e| e.exit());
        image_width = (image_height as f64 * aspect_ratio) as u32;
    } else {
        image_width = 900;
        image_height = 600;
    }

    (
        image_width,
        image_height,
        vfov,
        samples_per_pixel,
        max_depth,
        out_file_path,
    )
}

fn main() {
    let (
        image_width,
        image_height,
        vfov,
        samples_per_pixel,
        max_depth,
        out_file_path,
    ) = parse_args();

    // World
    let world = spheres_scene();

    // Camera
    let lookfrom: Point = (13.0, 2.0, 3.0).into();
    let lookat: Point = (0.0, 0.0, 0.0).into();
    let focus_dist = 10.0;
    let lens_radius = 0.05;

    let camera = Camera::new(
        lookfrom,
        lookat,
        image_width,
        image_height,
        vfov,
        focus_dist,
        lens_radius,
    );

    // Image
    let mut img = RgbImage::new(image_width, image_height);

    for row in (0..image_height).rev() {
        eprint!("\rscanlines remaining: {}", row);

        for col in 0..image_width {
            let u = col as f64 / (image_width - 1) as f64;
            let v = row as f64 / (image_height - 1) as f64;

            let pixel = img.get_pixel_mut(col, row);
            *pixel = camera
                .beam(samples_per_pixel, u, v)
                .color(&world, max_depth)
                .into();
        }
    }

    eprintln!("\nDone!");

    // ImageBuffer counts pixels from top down, while our Camera conts rays
    // from down up, so result image must be flipped
    image::imageops::flip_vertical(&img)
        .save(out_file_path)
        .unwrap();
}
