use crate::hit::*;
use crate::material::*;
use crate::Ray;

#[derive(Copy, Clone)]
pub struct Color {
    pub r: f64,
    pub g: f64,
    pub b: f64,
}

pub trait Colorable {
    fn color(&mut self, world: &[Sphere], max_depth: usize) -> Color;
}

impl Colorable for Ray {
    fn color(&mut self, world: &[Sphere], max_depth: usize) -> Color {
        let mut color: Color = (1.0, 1.0, 1.0).into();
        let mut depth = 0;

        while let Some(hit) = world.hit(self, 1e-10, f64::MAX) {
            let attenuation;

            match hit.material {
                Material::Lambertian { albedo } => {
                    attenuation = albedo;
                }
                Material::Metal { albedo, .. } => {
                    attenuation = albedo;
                }
                Material::Dielectric { .. } => {
                    attenuation = (1.0, 1.0, 1.0).into();
                }
            }

            if let Some(scattered_ray) = self.scatter(&hit) {
                color = attenuation * color;
                *self = scattered_ray;
                depth += 1;
            } else {
                return (0.0, 0.0, 0.0).into();
            }

            if depth > max_depth {
                return (0.0, 0.0, 0.0).into();
            }
        }

        // blue-white gradient depending on ray's Y coordinate
        let t = 0.5 * (self.direction.unit_vector().y + 1.0);
        let sky_color = Color {
            r: 1.0 - t + 0.5 * t,
            g: 1.0 - t + 0.7 * t,
            b: 1.0 - t + 1.0 * t,
        };

        color * sky_color
    }
}

impl Colorable for Vec<Ray> {
    fn color(&mut self, world: &[Sphere], max_depth: usize) -> Color {
        let mut beam_color: Color = (0.0, 0.0, 0.0).into();

        for ray in self.as_mut_slice() {
            beam_color = beam_color + ray.color(world, max_depth);
        }

        beam_color / self.len() as f64
    }
}
use std::fmt;

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let r = 256.0 * self.r.sqrt();
        let g = 256.0 * self.g.sqrt();
        let b = 256.0 * self.b.sqrt();

        write!(f, "{} {} {}", r as u8, g as u8, b as u8)
    }
}

use std::ops::Add;

impl Add for Color {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        (self.r + other.r, self.g + other.g, self.b + other.b).into()
    }
}

use std::ops::Div;

impl Div<f64> for Color {
    type Output = Self;

    fn div(self, other: f64) -> Self {
        (self.r / other, self.g / other, self.b / other).into()
    }
}

use std::ops::Mul;

impl Mul<Color> for Color {
    type Output = Self;

    fn mul(self, other: Color) -> Self {
        (self.r * other.r, self.g * other.g, self.b * other.b).into()
    }
}

impl From<(f64, f64, f64)> for Color {
    fn from(c: (f64, f64, f64)) -> Self {
        Color {
            r: c.0,
            g: c.1,
            b: c.2,
        }
    }
}

use image::Rgb;

impl From<Color> for Rgb<u8> {
    fn from(c: Color) -> Self {
        let r = 256.0 * c.r.sqrt();
        let g = 256.0 * c.g.sqrt();
        let b = 256.0 * c.b.sqrt();

        Rgb([r as u8, g as u8, b as u8])
    }
}
