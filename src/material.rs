use crate::vector::*;
use crate::Color;
use crate::HitRecord;
use crate::Ray;

use rand::random;

#[derive(Copy, Clone)]
pub enum Material {
    Lambertian { albedo: Color },
    Metal { albedo: Color, fuzz: f64 },
    Dielectric { refraction_idx: f64 },
}

pub trait Scatter {
    fn scatter(&self, hit: &HitRecord) -> Option<Ray>;
}

fn reflect(v: Vector, n: Vector, cosine: f64) -> Vector {
    v + 2.0 * cosine * n
}

fn refract(v: Vector, n: Vector, cosine: f64, eta: f64) -> Vector {
    let out_perp = eta * (v + cosine * n);
    let out_para = -(1.0 - out_perp.length_squared()).abs().sqrt() * n;

    out_perp + out_para
}

fn cannot_refract(cosine: f64, ref_idx: f64) -> bool {
    let sine = (1.0 - cosine.powi(2)).sqrt();
    if ref_idx * sine > 1.0 {
        return true;
    }

    let r0 = ((1.0 - ref_idx) / (1.0 + ref_idx)).powi(2);

    r0 + (1.0 - r0) * (1.0 - cosine).powi(5) > random()
}

impl Scatter for Ray {
    fn scatter(&self, hit: &HitRecord) -> Option<Ray> {
        match hit.material {
            Material::Lambertian { .. } => {
                let scattered_direction =
                    hit.normal + random::<Vector>().unit_vector();

                if scattered_direction.near_zero() {
                    None
                } else {
                    Some(Ray {
                        origin: hit.point,
                        direction: scattered_direction,
                    })
                }
            }
            Material::Metal { fuzz, .. } => {
                let unit_direction = self.direction.unit_vector();
                let cos_theta = dot_product(&(-unit_direction), &hit.normal);
                let reflected_direction =
                    reflect(unit_direction, hit.normal, cos_theta)
                        + fuzz * random::<Vector>().unit_vector();

                if dot_product(&reflected_direction, &hit.normal)
                    .is_sign_positive()
                {
                    Some(Ray {
                        origin: hit.point,
                        direction: reflected_direction,
                    })
                } else {
                    None
                }
            }
            Material::Dielectric { refraction_idx } => {
                let refract_ratio;
                let unit_direction = self.direction.unit_vector();
                let mut normal = hit.normal;
                let mut cos_theta = dot_product(&unit_direction, &normal);

                if cos_theta.is_sign_positive() {
                    normal = -normal;
                    refract_ratio = refraction_idx;
                } else {
                    cos_theta = -cos_theta;
                    refract_ratio = 1.0 / refraction_idx;
                }

                let direction;

                if cannot_refract(cos_theta, refract_ratio) {
                    direction = reflect(unit_direction, normal, cos_theta);
                } else {
                    direction = refract(
                        unit_direction,
                        normal,
                        cos_theta,
                        refract_ratio,
                    );
                }

                Some(Ray {
                    origin: hit.point,
                    direction,
                })
            }
        }
    }
}
