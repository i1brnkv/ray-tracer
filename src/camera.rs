use crate::vector::*;
use crate::Ray;
use rand::thread_rng;
use rand::Rng;

pub struct Camera {
    origin: Point,
    horizontal: Vector,
    vertical: Vector,
    low_left_corner: Vector,
    lens_radius: f64,
    image_width: u32,
    image_height: u32,
}

impl Camera {
    pub fn new(
        lookfrom: Point, lookat: Point, image_width: u32, image_height: u32,
        vfov: f64, focus_dist: f64, lens_radius: f64,
    ) -> Camera {
        let aspect_ratio = image_width as f64 / image_height as f64;
        let viewport_height =
            (vfov.to_radians() / 2.0).tan() * focus_dist * 2.0;
        let viewport_width = viewport_height * aspect_ratio;

        let vup: Vector = (0.0, 1.0, 0.0).into();

        let w = (lookfrom - lookat).unit_vector();
        let u = cross_product(&vup, &w).unit_vector();
        let v = cross_product(&w, &u);

        let origin = lookfrom;
        let horizontal = viewport_width * u;
        let vertical = viewport_height * v;
        let focal = focus_dist * w;
        let low_left_corner =
            origin - horizontal / 2.0 - vertical / 2.0 - focal;

        Camera {
            origin,
            horizontal,
            vertical,
            low_left_corner,
            lens_radius,
            image_width,
            image_height,
        }
    }

    fn ray(&self, u: f64, v: f64) -> Ray {
        let mut rng = thread_rng();
        let offset = self.lens_radius
            * (rng.gen_range(-1.0..1.0) * self.horizontal.unit_vector()
                + rng.gen_range(-1.0..1.0) * self.vertical.unit_vector());

        Ray {
            origin: self.origin + offset,
            direction: self.low_left_corner
                + u * self.horizontal
                + v * self.vertical
                - self.origin
                - offset,
        }
    }

    pub fn beam(&self, ray_num: usize, u: f64, v: f64) -> Vec<Ray> {
        let mut beam = Vec::with_capacity(ray_num);
        let mut rng = thread_rng();

        for _ in 0..ray_num {
            let u_rand = rng.gen::<f64>() / (self.image_width - 1) as f64;
            let v_rand = rng.gen::<f64>() / (self.image_height - 1) as f64;

            beam.push(self.ray(u + u_rand, v + v_rand));
        }

        beam
    }
}
