# Ray Tracer in One Weekend

This is my implementation of great tutorial
["Ray Tracing in One Weekend"](https://raytracing.github.io/books/RayTracingInOneWeekend.html)
writen in Rust but for much longer than one weekend.

## How-To Build

`cargo build` for debug build.

`cargo build --release` for release build.

## How-To Run

`cargo run -- [OPTIONS]` for running debug version.

`cargo run --release -- [OPTIONS]` for running release version.

### Note

Don't bother running debug version, it is REALLY slow.

### Options

```
FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
        --depth <depth>        maximum number of reflections to calculate for a single ray [default: 50]
        --fov <fov>            vertical angle of view [default: 22.0]
        --height <height>      image height in pixels, if not set will be `width / 1.5`, if both not set will be 600
        --out <out_file>       specify file to save the image in common format [default: out.png]
        --samples <samples>    number of rays per pixel [default: 10]
        --width <width>        image width in pixels, if not set will be `1.5 * height`, if both not set will be 900
```

## Result

### Spheres

![Spheres](img/spheres.png)
